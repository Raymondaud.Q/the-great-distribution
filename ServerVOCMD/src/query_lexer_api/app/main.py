# Author: Quentin RAYMONDAUD
# Open Source 
# UNLICENSED

from flask import Flask, request,session
from flask_session import Session
import urllib.request
import os
import ssl
import re

ssl._create_default_https_context = ssl._create_unverified_context

DATA_FOLDER = './data/'
app = Flask(__name__)
sess = Session()
app.config['DATA_FOLDER'] = DATA_FOLDER
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'
sess.init_app(app)

#            0        1       2         3       4          5           6         7  
general = ['joue', 'lance', 'arret', 'stop', 'pause', 'recherche', 'cherche', 'list']

# If general [ 0 | 1 ]
playorder = [ 'la musique', 'le titre', 'la chanson', 'le single']

# Elif general [ 4 | 5 | 6 | 7 ]
searchorder = ['par genre', 'par type', 'par style',
'par artiste', 'par auteur', 'par chanteur',
'par titre',
'tout', 'toutes les musiques', 'tous les titres']

def lex( query, reference ):
    for i in range(len(reference)):
        search = re.search(reference[i], query)
        if ( search ) :
            # Removes first string that match reference[i] (Racinized ref)
            query = query.replace(reference[i],'',1)
            # Remove the rest until the next word
            query = re.sub('^\S+',' ',query).strip()
            return query, i
    return query,-1


@app.route('/', methods=['GET'])
def home():
    return '''
    <!doctype html>
    <title>QueryLexer API</title>
    
    <h1> QueryLexer </h1>
    <form method=get action=/lexer enctype=multipart/form-data>
      <input type=text name=query>
      <input type=submit value=Query>
    </form>

    <h1> Add Sound Title </h1>
    <form method=get action=/addSoundTitle enctype=multipart/form-data>
      <input type=text name=title>
      <input type=submit value=Add Sound Title>
    </form>
    '''

@app.route('/lexer', methods=['GET'])
def query_lexer():
    cnt = 0
    if ( request.method == 'GET'):
        query = request.args.get('query').lower()
        query, index = lex(query, general)
        if ( index == 0 or index == 1 ):
            query, index = lex(query, playorder)
            return "PLAY:" + query
        elif ( index == 2 or index == 3 ):
            return "STOP"
        elif ( index == 4 ): 
            return "PAUSE"
        elif ( index > 4 and index < 8 ):
            query, index = lex(query, searchorder)
            if ( index >= 0 and index <= 2):
                return "SEARCH:KIND:" + query
            elif ( index >= 3 and index <= 5):
                return "SEARCH:AUTHOR:" + query
            elif ( index == 6):
                return "SEARCH:TITLE:" + query
            elif ( index  == -1 and len(query) > 0 ):
                return "SEARCH:TITLE:" + query
            else:
                return "LIST"
    return 'failed'

@app.route('/addSoundTitle', methods=['GET'])
def add_sound_title():
    if ( request.method == 'GET'):
        new_entry = request.args.get('title')
        with open(app.config['DATA_FOLDER']+'SoundList.txt', 'a') as file:
            file.write(new_entry+'\n')
            return new_entry + " added successfuly !"
    return "PAS COMPRENDU DSL"

if __name__ == "__main__":
    # Only for debugging while developing
    #app.run(host='0.0.0.0', debug=True, port=80)
    app.run()