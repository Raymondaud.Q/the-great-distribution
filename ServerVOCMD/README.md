# Build & Run W/O Docker

## Dependencies without docker

### Speech To Text API Dependencies
* pip install flask-session requests SpeechRecognition

### Language Processing API Dependencies
* pip install flask-session requests

## Run one API lonely

* export FLASK_APP=main.py  
* flask run

# Build & Run with Docker

![Presentation](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/raw/master/FR_PDF/VOCMD.png)

## Docker setup

* [Install docker](https://docs.docker.com/get-docker/)
* [Install docker compose](https://docs.docker.com/compose/install/)

## Run

* cd in the root of this repo
* sudo docker-compose build
* sudo docker-compose up
* Go to localhost:5000/ for QueryLexer Test Page
* Go to localhost:7000/ for SpeechToText Test Page
