# Android Great Radio Client

* This project is a forked from [Android Ice Demo](https://github.com/zeroc-ice/ice-demos/tree/3.7/java/android)  
* This project must be edited with Android Studio API 29 & 30 and built with provided gradle files  

You must open [`android`](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/Client/android) folder as a project in Android Studio.  

## Android Test Application

An up-to-date [`radio-debug.apk`](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/blob/master/Client/APK/radio-debug.apk) is provided for testing this app without installing the whole Android stack.   
You need to **allow Storage Permission manually** in your android application permission after having installed this application in order to perform any **File Upload**.   
The **microphone permission is asked** to the user, you need to accept it in order to use our **SpeechToText Service**.  

### Configuration 

Once you've started [`The Great Radio Server`](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/) and installed this app, you must edit the in-app configuration in order to match you server ip.  

* Click the config button at the top-right 
* Enter the Great Radio Server IP
* Press Back 
* Now you can do an empty query in order to get the whole sound list

Once the configuration is done, you can open and close this application without loosing your configuration.

## Documentation

A documentation has been generated automatically, feel free to look at [`Doc`](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/Client/Doc) folder before going further in the code.  
