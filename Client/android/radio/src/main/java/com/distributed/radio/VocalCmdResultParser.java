package com.distributed.radio;

import android.util.Log;

import com.zeroc.demos.android.hello.SoundLibrary.Sound;

public class VocalCmdResultParser {
    public static int getIndexSoundToPlay(Sound[] soundList, String result ){
        result = result.replace("PLAY:", "");
        String[] words = result.split("\\s+");
        int[] ranking = new int[soundList.length];
        for ( String word : words ) {
            word = word.toUpperCase();
            for ( int soundI = 0; soundI < soundList.length; soundI++ ){
                Log.d("getIndexSoundToPlay: ", word);
                if ( soundList[soundI].title.toUpperCase().contains(word) )
                    ranking[soundI] += 1;
                if ( soundList[soundI].kind.toUpperCase().contains(word) )
                    ranking[soundI] += 1;
                if ( soundList[soundI].author.toUpperCase().contains(word) )
                    ranking[soundI] += 1;
            }
        }

        if ( soundList.length < 1) {
            return -1;
        } else {
            int max = -10000;
            int index = -1;
            for (int i = 0; i < ranking.length; i++)
                if (ranking[i] > max) {
                    max = ranking[i];
                    index = i;
                }
            return index;
        }
    }
}
