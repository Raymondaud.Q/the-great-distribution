package com.distributed.radio;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


class QueryLexerAPIDoQuery extends AsyncTask<String, Void, String> {

    protected String doInBackground(String... urls) {
        try {
            URL url = new URL(IceBreaker.HOSTIP+":5000/lexer?query="+urls[0]);
            HttpURLConnection connection = (HttpURLConnection)  url.openConnection();
            // Now it's "open", we can set the request method, headers etc.
            connection.setRequestProperty("accept", "application/json");
            // This line makes the request
            InputStream responseStream = connection.getInputStream();
            StringBuilder textBuilder = new StringBuilder();
            try (Reader reader = new BufferedReader(new InputStreamReader
                    (responseStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
                int c; // = 0
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
                return textBuilder.toString();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "empty";
    }
}