package com.distributed.radio;

import android.app.Activity;
import android.app.Dialog;
import android.widget.Button;
import android.widget.EditText;
import com.zeroc.demos.android.hello.SoundLibrary.Sound;
import com.zeroc.hello.R;

public class UploadActivity {
    Activity activity;

    public void showDialog(MainActivity mainActivity, IceBreaker _app){
        this.activity = mainActivity;
        final Dialog dialog = new Dialog(this.activity);
        dialog.setContentView(R.layout.sound_form);
        final EditText title = dialog.findViewById(R.id.soundTitle);
        final EditText author = dialog.findViewById(R.id.soundAuthor);
        final EditText kind = dialog.findViewById(R.id.soundKind);
        Button uploadBtn = dialog.findViewById(R.id.validateUpload);
        uploadBtn.setOnClickListener(view -> {
            FileChooser fileChooser = new FileChooser(this.activity);
            fileChooser.setFileListener(file -> {
                String filename = file.getAbsolutePath();
                Sound toUpload = new Sound();
                toUpload.title = title.getText().toString();
                toUpload.author = author.getText().toString();
                toUpload.kind = kind.getText().toString();
                _app.uploadSoundAsync(toUpload, filename, mainActivity);
                dialog.dismiss();
                fileChooser.dismiss();
            });
            fileChooser.setExtension("mp3");
            fileChooser.showDialog();
        });
        dialog.show();
    }
}
