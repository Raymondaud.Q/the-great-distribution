package com.distributed.radio;

import android.app.Activity;
import android.app.Dialog;
import android.widget.Button;
import android.widget.EditText;
import com.zeroc.demos.android.hello.SoundLibrary.Sound;
import com.zeroc.hello.R;

public class SoundInfoActivity {
    Activity activity;

    public void showDialog(MainActivity mainActivity, IceBreaker _app, Sound sound){
        this.activity = (Activity) mainActivity;
        final Dialog dialog = new Dialog(this.activity);
        dialog.setContentView(R.layout.sound_inform);
        final EditText title = dialog.findViewById(R.id.soundTitle);
        final EditText author = dialog.findViewById(R.id.soundAuthor);
        final EditText kind = dialog.findViewById(R.id.soundKind);
        title.setText(sound.title.replaceAll("_", " "));
        author.setText(sound.author.replaceAll("_", " "));
        kind.setText(sound.kind.replaceAll("_", " "));

        Button renameBtn = dialog.findViewById(R.id.validateEdit);
        renameBtn.setOnClickListener(view -> {
            Sound toUpload = new Sound();
            toUpload.title = title.getText().toString();
            toUpload.author = author.getText().toString();
            toUpload.kind = kind.getText().toString();
            _app.renameSoundAsync(sound, toUpload, mainActivity);
            dialog.dismiss();
        });

        Button removeBtn = dialog.findViewById(R.id.validateRemove);
        removeBtn.setOnClickListener(view -> {
            _app.removeSoundAsync(sound, mainActivity);
            dialog.dismiss();
        });
        dialog.show();
    }
}
