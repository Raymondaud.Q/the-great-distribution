package com.distributed.radio;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;

import com.zeroc.Ice.LocalException;
import com.zeroc.demos.android.hello.SoundLibrary.Sound;
import com.zeroc.hello.R;

import java.io.File;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    static ProgressDialog progress;
    public static final String ERROR_TAG = "error";
    private static final String NET_KEY = "net";
    private static final String BUNDLE_DISCOVERY_KEY = "disco";
    private static final String DEFAULT_HOST = "http://127.0.0.1";
    private static final String HOSTNAME_KEY = "host";
    private static final String BUNDLE_KEY_MODE = "zeroc:mode";
    private static Button killVLC;
    private static Button playPauseVLC;
    private static Handler _handler;
    private IceBreaker _app =  (IceBreaker) getApplication();
    private ArrayAdapter<String> soundAdapter = null;
    private ListView listView = null;
    private static final String [] permissions = {Manifest.permission.RECORD_AUDIO};
    private static boolean permissionToRecordAccepted = false;
    private static boolean mStartRecording = true;
    private static MediaRecorder recorder;
    private String fileName = null;
    private Button speechToTextBtn;
    private EditText searchBar;


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION)
            permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        if (!permissionToRecordAccepted ) finish();
    }

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
            speechToTextBtn.setText(R.string.vocal_cmd_stop);
        } else {
            stopRecording();
            startPlaying();
            speechToTextBtn.setText(R.string.vocal_cmd_default);
        }
    }

    private void startPlaying() {
        MediaPlayer player = new MediaPlayer();
        try {
            player.setDataSource(fileName);
            player.prepare();
            player.start();
            Context mContext = this;
            // source : https://stackoverflow.com/questions/20235898/how-to-get-files-in-cache-directory-in-android
            File dir = new File(mContext.getCacheDir().getAbsolutePath());
            if (dir.exists()) {
                for (File f : dir.listFiles()) {
                    if ( f.getName().equals(fileName) ){
                        IceBreaker.speechToTextQuery(_app.getHost(), f.getCanonicalPath());
                    }
                }
            }
        } catch (Exception e) {
            Log.e("Player", "ERROR : prepare() failed - "+ e.getMessage());
        }
    }


    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(AudioFormat.ENCODING_PCM_16BIT);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(fileName);

        try {
            recorder.prepare();
        } catch (Exception e) {
            Log.e("RECORDER", " ERROR : prepare() failed - " + e.getMessage());
        }

        recorder.start();
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        recorder = null;
        Log.d("Speech 2 Text = ",_app.speechToTextQuery(_app.getHost(),fileName));
    }


    public void startVlc( String streamUrl, View view) {
        if ( streamUrl != null && ! streamUrl.equals("ERROR")) {
            Intent intent = new Intent( view.getContext(), VlcActivity.class );
            view.getContext().startActivity(intent);
        }
    }

    public static class ErrorDialogFragment extends DialogFragment
    {
        public static ErrorDialogFragment newInstance(String message, boolean fatal) {
            ErrorDialogFragment frag = new ErrorDialogFragment();
            Bundle args = new Bundle();
            args.putString("message", message);
            args.putBoolean("fatal", fatal);
            frag.setArguments(args);
            return frag;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            assert getArguments() != null;
            builder.setTitle("Error")
                   .setMessage(getArguments().getString("message"))
                   .setPositiveButton("Ok", (dialog, whichButton) -> {
                       if(getArguments().getBoolean("fatal"))
                           Objects.requireNonNull(getActivity()).finish();
                   });
            return builder.create();
        }
    }

    // These two arrays match.
    private final static DeliveryMode[] DELIVERY_MODES = {
        DeliveryMode.TWOWAY_SECURE,
        DeliveryMode.TWOWAY,
        DeliveryMode.ONEWAY,
        DeliveryMode.ONEWAY_BATCH,
        DeliveryMode.ONEWAY_SECURE,
        DeliveryMode.ONEWAY_SECURE_BATCH,
        DeliveryMode.DATAGRAM,
        DeliveryMode.DATAGRAM_BATCH,
    };

    public void stopSound(){
        VlcActivity.destroy();
        killVLC.setVisibility(View.INVISIBLE);
        playPauseVLC.setVisibility(View.INVISIBLE);
    }

    public void pauseSound(){
        VlcActivity.pause = ! VlcActivity.pause;
        if ( VlcActivity.pause )
            playPauseVLC.setBackgroundResource( android.R.drawable.ic_media_play );
        else
            playPauseVLC.setBackgroundResource( android.R.drawable.ic_media_pause );
        VlcActivity.pauseMedia();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        MainActivity.progress = new ProgressDialog(this);
        final SharedPreferences prefs = getSharedPreferences(NET_KEY, MODE_PRIVATE);
        _app = (IceBreaker) getApplication();
        if ( ! prefs.getBoolean(BUNDLE_DISCOVERY_KEY, false))
            _app.setHost(prefs.getString(HOSTNAME_KEY, DEFAULT_HOST));
        else
            _app.setUseDiscovery(true);
        int deliveryMode = prefs.getInt(BUNDLE_KEY_MODE, 0);
        _app.setDeliveryMode( DELIVERY_MODES[deliveryMode]);

        final Button getSoundListBtn = findViewById(R.id.soundList);
        final Button uploadBtn = findViewById(R.id.upload);
        final Button netConfigBtn = findViewById(R.id.config);
        final Spinner spinner = findViewById(R.id.spinner);
        killVLC = findViewById(R.id.killVLC);
        playPauseVLC = findViewById(R.id.playpause);
        speechToTextBtn = findViewById(R.id.vocalCmd);
        fileName = getExternalCacheDir().getAbsolutePath();
        fileName += "/command.aac";

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        speechToTextBtn.setOnClickListener( view -> {
            onRecord(mStartRecording);
            if (mStartRecording) {
                searchBar.setHint(R.string.search_recording);
            } else {
                searchBar.setHint(R.string.search_default);
            }
            mStartRecording = !mStartRecording;
        });


        if ( VlcActivity.running){
            playPauseVLC.setBackgroundResource( android.R.drawable.ic_media_pause );
            killVLC.setBackgroundResource(android.R.drawable.ic_delete);
            killVLC.setVisibility(View.VISIBLE);
            playPauseVLC.setVisibility(View.VISIBLE);
        } else {
            killVLC.setVisibility(View.INVISIBLE);
            playPauseVLC.setVisibility(View.INVISIBLE);
        }

        killVLC.setOnClickListener(v -> {
            VlcActivity.destroy();
            killVLC.setVisibility(View.INVISIBLE);
            playPauseVLC.setVisibility(View.INVISIBLE);
        });

        playPauseVLC.setOnClickListener(v -> {
            VlcActivity.pause = ! VlcActivity.pause;
            if ( VlcActivity.pause )
                playPauseVLC.setBackgroundResource( android.R.drawable.ic_media_play );
            else
                playPauseVLC.setBackgroundResource( android.R.drawable.ic_media_pause );
            VlcActivity.pauseMedia();
        });

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.search_array, R.layout.query_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _app.setQueryType(i-1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        searchBar = findViewById(R.id.searchView);
        netConfigBtn.setOnClickListener(view -> {
            Intent intent = new Intent(view.getContext(), ConfigActivity.class);
            view.getContext().startActivity(intent);});

        listView = findViewById(R.id.sounds);
        soundAdapter = new ArrayAdapter<>(this, R.layout.item, R.id.soundItem);
        this.populate(IceBreaker.soundList);
        listView.setOnItemLongClickListener((adapterView, view, i, l) -> {
            SoundInfoActivity myDialog = new SoundInfoActivity();
            myDialog.showDialog( MainActivity.this, _app, IceBreaker.soundList[i]);
            return true;
        });

        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            progress.setTitle(getString(R.string.opening_stream));
            progress.show();
            _app.playSound( IceBreaker.soundList[i], this, view );
        });

        getSoundListBtn.setOnClickListener(v -> {
            String searchText =  searchBar.getText().toString();
            String spinnerText =  spinner.getSelectedItem().toString();
            if( searchText.equals("")) {
                _app.getSoundListAsync(this);
            } else if ( ! spinnerText.equals("Command") ) {
                _app.soundQueryAsync(searchText, this);
                if ( IceBreaker.soundList != null ) {
                    soundAdapter.clear();
                    String strSound;
                    for ( Sound s : IceBreaker.soundList){
                        strSound = (s.author+" - "+s.title+"\n#"+s.kind).replaceAll("_", " ");
                        soundAdapter.add(strSound);
                    }
                    listView.setAdapter(soundAdapter);
                }
            } else {
                _app.simpleQuery(searchText, this, v);
            }
        });

        uploadBtn.setOnClickListener(v -> {
            UploadActivity myDialog = new UploadActivity();
            myDialog.showDialog( MainActivity.this, _app);
        });

        _handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message m) {
                switch(m.what) {
                    case IceBreaker.MSG_WAIT: {
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        break;
                    }

                    case IceBreaker.MSG_READY: {
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        IceBreaker.MessageReady ready = (IceBreaker.MessageReady)m.obj;
                        if(ready.ex != null) {
                            LocalException ex = ready.ex;
                            DialogFragment dialog = ErrorDialogFragment.newInstance(ex.toString(), true);
                            dialog.show(getSupportFragmentManager(), ERROR_TAG);
                        }
                        break;
                    }

                    case IceBreaker.MSG_EXCEPTION: {
                        progress.dismiss();
                        LocalException ex = (LocalException)m.obj;
                        DialogFragment dialog = ErrorDialogFragment.newInstance(ex.toString(), false);
                        dialog.show(getSupportFragmentManager(), ERROR_TAG);
                        break;
                    }

                    case IceBreaker.MSG_RESPONSE: {
                        progress.dismiss();
                        break;
                    }

                    case IceBreaker.MSG_SENT: {
                        progress.setTitle(R.string.waiting_response);
                        progress.show();
                        break;
                    }

                    case IceBreaker.MSG_SENDING: {
                        progress.setTitle(R.string.sending_request);
                        progress.show();
                        break;
                    }
                }
            }
        };
    }

    public void setPlayerButtonsVisible(){
        playPauseVLC.setBackgroundResource( android.R.drawable.ic_media_pause );
        killVLC.setBackgroundResource(android.R.drawable.ic_delete);
        killVLC.setVisibility(View.VISIBLE);
        playPauseVLC.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        _app.setHandler(_handler);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final SharedPreferences prefs = getSharedPreferences(NET_KEY, MODE_PRIVATE);

        _app = (IceBreaker) getApplication();
        // Setup the defaults.
        if ( ! prefs.getBoolean(BUNDLE_DISCOVERY_KEY, false))
            _app.setHost(prefs.getString(HOSTNAME_KEY, DEFAULT_HOST));
        else
            _app.setUseDiscovery(true);

        int deliveryMode = prefs.getInt(BUNDLE_KEY_MODE, 0);
        _app.setDeliveryMode( DELIVERY_MODES[deliveryMode]);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        _app.setHandler(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _app.setHandler(null);
    }

    public void populate(Sound[] sounds){
        if ( sounds != null ) {
            soundAdapter.clear();
            for ( Sound s : sounds)
                soundAdapter.add(s.author.replaceAll("_", " ") + " - " +
                        s.title.replaceAll("_", " ") +
                        "\n#"+s.kind.replaceAll("_", " ") );
            listView.setAdapter(soundAdapter);
        }
    }

}
