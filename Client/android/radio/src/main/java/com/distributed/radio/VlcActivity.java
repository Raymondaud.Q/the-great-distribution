package com.distributed.radio;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.zeroc.demos.android.hello.SoundLibrary.Sound;
import com.zeroc.hello.R;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.VLCVideoLayout;

import java.util.ArrayList;

public class VlcActivity extends AppCompatActivity  {
    private static IceBreaker _app;
    private static LibVLC mLibVLC = null;
    private static MediaPlayer mMediaPlayer = null;
    private static VLCVideoLayout mVideoLayout = null;
    private static Button pauseBtn = null;
    private Button shareBtn = null;
    static boolean pause = false;
    static boolean running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ( running && mMediaPlayer != null && mMediaPlayer.isPlaying()){
            mMediaPlayer.stop();
        }
        _app = (IceBreaker) getApplication();
        setContentView(R.layout.activity_video_vlc);
        mVideoLayout = findViewById(R.id.video_layout);
        pauseBtn = findViewById(R.id.pauseBtn);
        shareBtn = findViewById(R.id.share);
        TextView details = findViewById(R.id.playing);
        Sound currentSound = IceBreaker.getCurrentSound();
        final ArrayList<String> args = new ArrayList<>();
        args.add("-vvv");
        mLibVLC = new LibVLC(this, args);
        mMediaPlayer = new MediaPlayer(mLibVLC);
        String detailsContent = currentSound.author + " - " + currentSound.title + " #" + currentSound.kind;
        details.setText( detailsContent.replaceAll("_", " ") );
        details.setMovementMethod(new ScrollingMovementMethod());
        pauseBtn.setOnClickListener( v -> {
            pause = ! pause;
            if ( pause )
                pauseBtn.setBackgroundResource( android.R.drawable.ic_media_play );
            else
                pauseBtn.setBackgroundResource( android.R.drawable.ic_media_pause );
            pauseMedia();
        });
        shareBtn.setOnClickListener( v -> {
            String mrlToShare = _app.getHost()+IceBreaker.streamUrl;
            new AlertDialog.Builder(this)
                .setTitle("Share "+ detailsContent)
                .setMessage(mrlToShare)
                .setPositiveButton("Copy", (dialog, which) -> {
                    if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
                        clipboard.setText(mrlToShare);
                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", mrlToShare);
                        clipboard.setPrimaryClip(clip);
                    }
                })
                .setNegativeButton("Cancel", null)
                .setIcon(android.R.drawable.ic_menu_share)
                .show();
        });

    }

    public static void pauseMedia(){
        if ( pause ){
            mMediaPlayer.pause();
        } else {
            mMediaPlayer.play();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMediaPlayer.stop();
        mMediaPlayer.release();
        mLibVLC.release();
        running = false;
    }

    public static void destroy(){
        mMediaPlayer.stop();
        mMediaPlayer.release();
        mLibVLC.release();
        _app.stopSoundAsync();
        running = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        boolean USE_TEXTURE_VIEW = false;
        boolean ENABLE_SUBTITLES = true;
        mMediaPlayer.attachViews(mVideoLayout, null, ENABLE_SUBTITLES, USE_TEXTURE_VIEW);
        try {
            final Media media = new Media(mLibVLC, Uri.parse(_app.getHost()+IceBreaker.streamUrl));
            mMediaPlayer.setMedia(media);
            media.release();
        } catch (Exception e) {
            throw new RuntimeException("Invalid Stream");
        }
        mMediaPlayer.play();
        running = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //mMediaPlayer.stop();
        mMediaPlayer.detachViews();
    }
}

