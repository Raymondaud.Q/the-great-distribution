package com.distributed.radio;

import android.app.Application;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.zeroc.Ice.Communicator;
import com.zeroc.Ice.Connection;
import com.zeroc.Ice.InitializationData;
import com.zeroc.Ice.InvocationFuture;
import com.zeroc.Ice.LocalException;
import com.zeroc.Ice.ObjectPrx;
import com.zeroc.Ice.Util;
import com.zeroc.demos.android.hello.SoundLibrary.AuthenticatorPrx;
import com.zeroc.demos.android.hello.SoundLibrary.PlayerInterfacePrx;
import com.zeroc.demos.android.hello.SoundLibrary.Sound;
import com.zeroc.demos.android.hello.SoundLibrary.SoundInterfacePrx;
import com.zeroc.hello.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class IceBreaker extends Application {
    public static String HOSTIP ;
    private int queryType = 0;
    private final List<Message> _queue = new LinkedList<>();
    private Handler _uiHandler;
    private boolean _initialized;
    private Communicator _communicator;
    private SoundInterfacePrx _soundProxy = null;
    private PlayerInterfacePrx _playerProxy = null;
    private AuthenticatorPrx _authProxy = null;
    // private ComplexQueryInterfacePrx _queryProxy = null;
    // The current request if any.
    private InvocationFuture<?> _result;
    // The mode of the current request.
    private DeliveryMode _resultMode;
    private Handler _handler;
    // Proxy settings.
    private String _host;
    private boolean _useDiscovery;
    private int _timeout;
    private DeliveryMode _mode;
    private static Sound currentSound;
    public static Sound[] soundList;
    public static String streamUrl;
    public static final int MSG_WAIT = 0;
    public static final int MSG_READY = 1;
    public static final int MSG_EXCEPTION = 2;
    public static final int MSG_RESPONSE = 3;
    public static final int MSG_SENDING = 4;
    public static final int MSG_SENT = 5;

    public static Sound getCurrentSound(){
        return currentSound;
    }

    public String getHost() {
        return this._host;
    }

    static class MessageReady {
        MessageReady(Communicator c, LocalException e){
            communicator = c;
            ex = e;
        }
        Communicator communicator;
        LocalException ex;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _uiHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message m) {
                if(m.what == MSG_READY) {
                    MessageReady ready = (MessageReady)m.obj;
                    _initialized = true;
                    _communicator = ready.communicator;
                }
                else if(m.what == MSG_EXCEPTION || m.what == MSG_RESPONSE)
                    _result = null;

                Message copy = new Message();
                copy.copyFrom(m);

                if(_handler != null)
                    _handler.sendMessage(copy);
                else
                    _queue.add(copy);


            }
        };

        WifiManager _wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        //
        // On some devices, a multicast lock must be acquired otherwise multicast packets are discarded.
        // The lock is initially unacquired.
        //
        WifiManager.MulticastLock _lock = _wifiManager.createMulticastLock("com.zeroc.demos.android.hello");
        _lock.acquire();

        // SSL initialization can take some time. To avoid blocking the
        // calling thread, we perform the initialization in a separate thread.
        new Thread(() -> {
            try {
                InitializationData initData = new InitializationData();
                initData.dispatcher = (Runnable runnable, Connection connection) -> _uiHandler.post(runnable);
                initData.properties = Util.createProperties();
                initData.properties.setProperty("Ice.Plugin.IceDiscovery", "IceDiscovery:createIceDiscovery");
                initData.properties.setProperty("Ice.ImplicitContext","Shared");
                initData.properties.setProperty("Ice.Trace.Network", "3");
                initData.properties.setProperty("IceSSL.Trace.Security", "3");
                initData.properties.setProperty("IceSSL.KeystoreType", "BKS");
                initData.properties.setProperty("IceSSL.TruststoreType", "BKS");
                initData.properties.setProperty("IceSSL.Password", "password");
                initData.properties.setProperty("Ice.Plugin.IceSSL", "com.zeroc.IceSSL.PluginFactory");
                initData.properties.setProperty("Ice.Plugin.IceDiscovery", "com.zeroc.IceDiscovery.PluginFactory");
                initData.properties.setProperty("Ice.Default.Package", "com.zeroc.demos.android.hello");
                //
                // We need to postpone plug-in initialization so that we can configure IceSSL
                // with a resource stream for the certificate information.
                //
                initData.properties.setProperty("Ice.InitPlugins", "0");
                Communicator c = Util.initialize(initData);
                //
                // Now we complete the plug-in initialization.
                //
                com.zeroc.IceSSL.Plugin plugin = (com.zeroc.IceSSL.Plugin)c.getPluginManager().getPlugin("IceSSL");
                //
                // Be sure to pass the same input stream to the SSL plug-in for
                // both the keystore and the truststore. This makes startup a
                // little faster since the plug-in will not initialize
                // two keystores.
                //
                java.io.InputStream certs = getResources().openRawResource(R.raw.client);
                plugin.setKeystoreStream(certs);
                plugin.setTruststoreStream(certs);
                c.getPluginManager().initializePlugins();
                _uiHandler.sendMessage(Message.obtain(_uiHandler, MSG_READY, new MessageReady(c, null)));
            }catch(LocalException e) {
                _uiHandler.sendMessage(Message.obtain(_uiHandler, MSG_READY, new MessageReady(null, e)));
            }
        }).start();
    }

    /** Called when the application is stopping. */
    @Override
    public void onTerminate() {
        super.onTerminate();
        if(_communicator != null)
            _communicator.destroy();
    }

    void setHandler(Handler handler) {
        // Nothing to do in this case.
        if(_handler != handler) {
            _handler = handler;
            if(_handler != null) {
                if(!_initialized)
                    _handler.sendMessage(_handler.obtainMessage(MSG_WAIT));
                else {
                    // Send all the queued messages.
                    while(!_queue.isEmpty())
                        _handler.sendMessage(_queue.remove(0));
                }
            }
        }
    }

    void setHost(String host) {
        flush();
        _host = host;
        IceBreaker.HOSTIP = host;
        _soundProxy = null;
    }

    void setUseDiscovery(boolean b) {
        flush();
        _useDiscovery = b;
        _soundProxy = null;
    }

    void setTimeout(int timeout) {
        flush();
        _timeout = timeout;
        _soundProxy = null;
    }

    void setDeliveryMode(DeliveryMode mode) {
        flush();
        _mode = mode;
        _soundProxy = null;
    }

    void flush() {
        if(_soundProxy != null) {
            _soundProxy.ice_flushBatchRequestsAsync().whenComplete((result, ex) -> {
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
            });
        }
    }

    static class BooleanHolder {
        boolean value;
    }

    /* void shutdownAsync() {
        try {
            updateProxy();
            if(_soundProxy == null || _result != null)
                return;

            _resultMode = _mode;
            final BooleanHolder response = new BooleanHolder();
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
            CompletableFuture<Void> r = _soundProxy.shutdownAsync();
            r.whenComplete((result, ex) -> {
                // There is no ordering guarantee between sent, response/exception.
                response.value = true;
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                else
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
            });

            _result = Util.getInvocationFuture(r);
            _result.whenSent((sentSynchronously, ex) -> {
                if(ex == null)
                {
                    if(_resultMode.isOneway())
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    else if(!response.value)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENT, _resultMode));
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    } */

    //
    // Called for a batch proxy.
    //https://doc.zeroc.com/ice/3.6/language-mappings/java-mapping/client-side-slice-to-java-mapping/java-mapping-for-operations#id-.JavaMappingforOperationsv3.6-Out-ParametersinJava
    void getToken() {
        try
        {
            updateProxy();
            if(_authProxy == null || _result != null )
                return;
            _authProxy.getTokenAsync().whenComplete((result, ex) -> {
                if(ex != null) {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                } else {
                    _communicator.getImplicitContext().put("accessToken", result );
                    // flush();
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    void getSoundListAsync(MainActivity mainActivity) {
        try {
            updateProxy();
            if(_soundProxy == null || _result != null)
                return;

            _resultMode = _mode;
            final BooleanHolder response = new BooleanHolder();
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
            CompletableFuture<Sound[]> r = _soundProxy.getSoundListAsync();
            r.whenComplete((result, ex) -> {
                // There is no ordering guarantee between sent, response/exception.
                response.value = true;
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                else {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    soundList = result;
                    mainActivity.populate(soundList);
                }
            });

            _result = Util.getInvocationFuture(r);
            _result.whenSent((sentSynchronously, ex) -> {
                if(ex == null) {
                    if(_resultMode.isOneway())
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    else if(!response.value)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENT, _resultMode));
                }
            });

        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    void soundQueryAsync(String query, MainActivity mainActivity){
        try {
            updateProxy();
            if(_soundProxy == null || _result != null)
                return;

            _resultMode = _mode;
            final BooleanHolder response = new BooleanHolder();
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
            CompletableFuture<Sound[]> r = _soundProxy.soundQueryAsync(this.queryType, query);
            r.whenComplete((result, ex) -> {
                // There is no ordering guarantee between sent, response/exception.
                response.value = true;
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                else {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    soundList = result;
                    mainActivity.populate(soundList);
                }
                // flush();
            });

            _result = Util.getInvocationFuture(r);
            _result.whenSent((sentSynchronously, ex) -> {
                if(ex == null) {
                    if(_resultMode.isOneway())
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    else if(!response.value)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENT, _resultMode));
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    void removeSoundAsync(Sound toRemove, MainActivity mainActivity){
        try {
            updateProxy();
            if(_soundProxy == null || _result != null)
                return;

            _resultMode = _mode;
            final BooleanHolder response = new BooleanHolder();
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
            CompletableFuture<Boolean> r = _soundProxy.removeSoundAsync(toRemove);
            r.whenComplete((result, ex) -> {
                // There is no ordering guarantee between sent, response/exception.
                response.value = true;
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                else {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    Sound[] arrayMinusOne = new Sound[soundList.length-1];
                    int y = 0;
                    for (Sound sound : soundList) {
                        if (sound != toRemove) {
                            arrayMinusOne[y] = sound;
                            y++;
                        }
                    }
                    soundList = arrayMinusOne;
                    mainActivity.populate(soundList);
                }
            });

            _result = Util.getInvocationFuture(r);
            _result.whenSent((sentSynchronously, ex) -> {
                if(ex == null) {
                    if(_resultMode.isOneway())
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    else if(!response.value)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENT, _resultMode));
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    void renameSoundAsync(Sound toRename, Sound newName, MainActivity mainActivity){
        try {
            updateProxy();
            if(_soundProxy == null || _result != null)
                return;

            _resultMode = _mode;
            final BooleanHolder response = new BooleanHolder();
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
            CompletableFuture<Boolean> r = _soundProxy.renameSoundAsync(toRename, newName);
            r.whenComplete((result, ex) -> {
                // There is no ordering guarantee between sent, response/exception.
                response.value = true;
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                else {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    for ( int i = 0; i < soundList.length ; i++ ){
                        if ( soundList[i] == toRename )
                            soundList[i] = newName;
                    }
                    mainActivity.populate(soundList);
                }
            });

            _result = Util.getInvocationFuture(r);
            _result.whenSent((sentSynchronously, ex) -> {
                if(ex == null) {
                    if(_resultMode.isOneway())
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    else if(!response.value)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENT, _resultMode));
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    void playSound(Sound sound, MainActivity mainActivity, View view) {
        try {
            updateProxy();
            if(_playerProxy == null || _result != null )
                return;_playerProxy.playSoundAsync(_communicator.getImplicitContext().get("accessToken"), sound).whenComplete((result, ex) -> {
                if(ex != null) {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                    streamUrl = "ERROR";
                } else {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    streamUrl = result;
                    currentSound = sound;
                    mainActivity.runOnUiThread(() -> {
                            mainActivity.setPlayerButtonsVisible();
                            mainActivity.startVlc(streamUrl, view);
                    });
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    // Refers to the ICE Query Lexer
    // Uncomment "QueryLexer" proxy and this function in Update Proxy function in order to use it
    /* void simpleQuery(String query, MainActivity mainActivity) {
        AtomicReference<String> qResult = null;
        try {
            updateProxy();
            if(_queryProxy == null || _result != null )
                return;_queryProxy.analyzeAsync(query).whenComplete((result, ex) -> {
                if(ex != null) {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                } else {
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    Log.d("analyzeQuery res : ", "\n\n\n" + result + "\n\n\n");
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    } */


    public String simpleQuery(String searchText, MainActivity mainActivity, View view) {
        String result = null;
        try {
            result = new QueryLexerAPIDoQuery().execute(searchText).get();
            if (result.contains("PLAY:")) {
                int indexSTP = VocalCmdResultParser.getIndexSoundToPlay(soundList, result);
                if (indexSTP != -1)
                    playSound(soundList[indexSTP], mainActivity, view);
                //else
                // TODO
            } else if ( result.contains("SEARCH") ){

                String kindMarker = "SEARCH:KIND:";
                String authorMarker = "SEARCH:AUTHOR:";
                String titleMarker = "SEARCH:TITLE:";
                int saveQueryType = this.queryType;

                if ( result.contains(titleMarker)) {
                    result = result.replace(titleMarker, "");
                    this.queryType = 0;
                } else if ( result.contains(authorMarker)) {
                    result = result.replace(authorMarker, "");
                    this.queryType = 1;
                } else if ( result.contains(kindMarker)) {
                    result = result.replace(kindMarker, "");
                    this.queryType = 2;
                }
                this.soundQueryAsync(result,mainActivity);
                this.queryType = saveQueryType;

            } else if ( result.contains("STOP") ){
                mainActivity.stopSound();
                this.stopSoundAsync();
            }
            else if ( result.contains("PAUSE") ) {
                mainActivity.pauseSound();
                VlcActivity.pauseMedia();
            } else if ( result.contains("LIST") ) {
                this.getSoundListAsync(mainActivity);
            }
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String speechToTextQuery(String url, String fileName){
        try {
            return new QueryVocalCmd().execute(fileName).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return "EMPTY";
    }


    void stopSoundAsync() {
        try {
            updateProxy();
            streamUrl = null;
            if(_playerProxy == null || _result != null)
                return;

            _resultMode = _mode;
            final BooleanHolder response = new BooleanHolder();
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
            CompletableFuture<Void> r = _playerProxy.stopSoundAsync(_communicator.getImplicitContext().get("accessToken"));
            r.whenComplete((result, ex) -> {
                // There is no ordering guarantee between sent, response/exception.
                response.value = true;
                if(ex != null)
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                else
                    _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
            });

            _result = Util.getInvocationFuture(r);
            _result.whenSent((sentSynchronously, ex) -> {
                if(ex == null) {
                    if(_resultMode.isOneway())
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                    else if(!response.value)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENT, _resultMode));
                }
            });
        } catch(LocalException ex) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
        }
    }

    void uploadSoundAsync(Sound sound, String filename, MainActivity mainActivity) {
        FileInputStream file = null;
        try {
            file = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        updateProxy();
        if (_soundProxy == null || _result != null)
            return;

        int chunkSize = 1024;
        int offset = 0;
        int numRequests = 5;
        int consumed;
        byte[] chunk = new byte[chunkSize];
        LinkedList<CompletableFuture<Integer>> results = new LinkedList<>();
        _resultMode = _mode;
        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_SENDING));
        try {
            while ( file !=  null && (consumed = file.read(chunk)) != -1) { // While not EOF
                CompletableFuture<Integer> r = _soundProxy.uploadSoundAsync(sound, offset, chunk);
                offset += consumed;
                r.whenComplete((result, ex) -> {
                    // There is no ordering guarantee between sent, response/exception.
                    if (ex != null)
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, ex));
                    else
                        _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_RESPONSE));
                });

                // Once there are more than numRequests, wait for the least
                // recent one to complete.
                while (results.size() > numRequests) {
                    CompletableFuture<Integer> res = results.getFirst();
                    results.removeFirst();
                    res.wait();
                }
            }
            // %23 = '#' char
            String queryLexerEntry  = sound.author.replaceAll("&","%26" ) + "-" + sound.title.replaceAll("&","%26" ) + "%23" + sound.kind.replaceAll("&","%26" );
            String result = new QueryLexerAPIAddSoundTitle().execute(queryLexerEntry).get();
            Log.d("uploadSoundAsync.QueryLexerAddTitle: ", result );
            mainActivity.runOnUiThread(() -> {
                if ( soundList == null )
                    soundList = new Sound[0];
                Sound[] appendedSoundList = new Sound[soundList.length+1];
                System.arraycopy(soundList, 0, appendedSoundList, 0, soundList.length);
                sound.author = sound.author.replaceAll(" ", "_");
                sound.kind = sound.kind.replaceAll(" ", "_");
                sound.title = sound.title.replaceAll(" ", "_");
                appendedSoundList[soundList.length] = sound;
                soundList = appendedSoundList;
                mainActivity.populate(soundList);
            });
        } catch (IOException | InterruptedException e) {
            _uiHandler.sendMessage(_uiHandler.obtainMessage(MSG_EXCEPTION, e));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // Wait for any remaining requests to complete.
        while (results.size() > 0) {
            CompletableFuture<Integer> res = results.getFirst();
            results.removeFirst();
            try {
                res.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateProxy() {
        if(_soundProxy != null && _playerProxy != null && _authProxy != null )
            return;
        String s, s2, s3; //s4;
        if(_useDiscovery) {
            s = "SoundCrud";
            s2 = "SoundPlayer";
            s3 = "authenticator";
            // s4 = "QueryLexer";
        } else {
            String editedHost = _host.replaceAll("http://", "");
            s = "soundcrud:tcp -h " + editedHost + " -p 10000:ssl -h " + editedHost + " -p 10001";
            s2 = "soundplayer:tcp -h " + editedHost + " -p 10002:ssl -h " + editedHost + " -p 10003";
            s3 = "authenticator:tcp -h " + editedHost + " -p 10004:ssl -h " + editedHost+ " -p 10005";
            // s4 = "QueryLexer@SoundLibAdapter:tcp -h " + _host + " -p 10000:ssl -h " + _host + " -p 10001";
        }
        if ( _communicator != null ) {
            ObjectPrx prx = _communicator.stringToProxy(s);
            ObjectPrx prx2 = _communicator.stringToProxy(s2);
            ObjectPrx prx3 = _communicator.stringToProxy(s3);
            // ObjectPrx prx4 = _communicator.stringToProxy(s4);

            if ( prx != null && prx2 != null && prx3 != null){
                prx = _mode.apply(prx);
                prx2 = _mode.apply(prx2);
                prx3 = _mode.apply(prx3);
                // prx4 = _mode.apply(prx4);
                if (_timeout != 0) {
                    prx = prx.ice_invocationTimeout(_timeout);
                    prx2 = prx.ice_invocationTimeout(_timeout);
                    prx3 = prx.ice_invocationTimeout(_timeout);
                    // prx4 = prx.ice_invocationTimeout(_timeout);
                }
                _soundProxy = SoundInterfacePrx.uncheckedCast(prx);
                _playerProxy = PlayerInterfacePrx.uncheckedCast(prx2);
                _authProxy = AuthenticatorPrx.uncheckedCast(prx3);
                getToken();
            }
        }
        // _queryProxy = ComplexQueryInterfacePrx.uncheckedCast(prx4);
    }

    public void setQueryType( int i ){ this.queryType = i;}

}
