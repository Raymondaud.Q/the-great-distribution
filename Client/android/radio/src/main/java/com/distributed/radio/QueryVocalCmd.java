package com.distributed.radio;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


public class QueryVocalCmd extends AsyncTask<String, Void, String> {

    protected String doInBackground(String... urls) {
        try {
            return new VocalCmd(IceBreaker.HOSTIP,urls[0]).flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "empty";
    }
}

class VocalCmd {

    HttpURLConnection connection;
    URL targetUrl;
    DataOutputStream request;
    String fileName;
    String path;

    // Inspiré de : https://riptutorial.com/android/example/14316/upload--post--file-using-httpurlconnection
    public VocalCmd(String path, String fileName) throws IOException {
        this.targetUrl = new URL(path+":7000/");
        this.connection = (HttpURLConnection) targetUrl.openConnection();

        String boundary =  "*****"+ Long.valueOf(System.currentTimeMillis()) +"*****";

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary="+boundary);
        this.request = new DataOutputStream(connection.getOutputStream());

        this.request.writeBytes("--" + boundary + "\r\n");
        this.request.writeBytes("Content-Disposition: form-data; name=\"description\"\r\n\r\n");
        this.request.writeBytes("VocalCmdRequest" + "\r\n");

        this.request.writeBytes("--" + boundary + "\r\n");
        this.request.writeBytes("Content-Disposition: form-data; name=\"file\"; filename=\"command.wav\"\r\n\r\n");
        byte[] file = fileToByteArray(fileName);
        if ( file != null ){
            this.request.write(file);
            this.request.writeBytes("\r\n");
            this.request.writeBytes("--" + boundary + "--\r\n");
        }  else
            throw new IOException("Fichier introuvable");
    }

    public String flush() throws IOException {
        this.request.flush();
        // int respCode = connection.getResponseCode();
        return  connection.getResponseMessage();
    }

    protected byte[] fileToByteArray ( String path ){
        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        BufferedInputStream buf = null;
        try {
            buf = new BufferedInputStream(new FileInputStream(file));
            try {
                buf.read(bytes, 0, bytes.length);
                buf.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        }
        return bytes;
    }

}
