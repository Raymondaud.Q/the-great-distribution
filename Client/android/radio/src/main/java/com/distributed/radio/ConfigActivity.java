package com.distributed.radio;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.zeroc.hello.R;

public class ConfigActivity extends AppCompatActivity {

    private static final String HOSTNAME_KEY = "host";
    private static final String NET_KEY = "net";
    private static final String BUNDLE_DISCOVERY_KEY = "disco";
    private static final String BUNDLE_KEY_MODE = "zeroc:mode";
    private static final String BUNDLE_KEY_TIMEOUT = "zeroc:timeout";
    private static final String BUNDLE_KEY_HOSTIP = "host";
    private IceBreaker _app;

    private final static DeliveryMode[] DELIVERY_MODES = {
            DeliveryMode.TWOWAY_SECURE,
            DeliveryMode.TWOWAY,
            DeliveryMode.ONEWAY,
            DeliveryMode.ONEWAY_BATCH,
            DeliveryMode.ONEWAY_SECURE,
            DeliveryMode.ONEWAY_SECURE_BATCH,
            DeliveryMode.DATAGRAM,
            DeliveryMode.DATAGRAM_BATCH,
    };

    private final static String[] DELIVERY_MODE_DESC = new String[]{
            "Twoway Secure",
            "Twoway",
            "Oneway",
            "Oneway Batch",
            "Oneway Secure",
            "Oneway Secure Batch",
            "Datagram",
            "Datagram Batch"
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        _app = (IceBreaker) getApplication();
        _app.flush();
        final EditText hostEditText = (EditText)findViewById(R.id.host);
        hostEditText.setText(_app.getHost());
        final CheckBox useDiscoveryCheckBox = (CheckBox)findViewById(R.id.useDiscovery);
        final TextView timeoutTextView = (TextView)findViewById(R.id.timeoutView);
        final SeekBar timeoutSeekBar = (SeekBar)findViewById(R.id.timeout);
        final SharedPreferences prefs = getSharedPreferences(NET_KEY, MODE_PRIVATE);
        final Spinner modeSpinner = (Spinner)findViewById(R.id.mode);
        final Button regenToken = (Button) findViewById(R.id.newToken);
        final ProgressDialog progress = new ProgressDialog(this);
        
        regenToken.setOnClickListener(view -> {
            progress.setTitle(R.string.get_token);
            progress.show();
            _app.getToken();
            progress.dismiss();
        });

        useDiscoveryCheckBox.setOnClickListener(v -> {
            final boolean checked = ((CheckBox)v).isChecked();
             _app.setUseDiscovery(checked);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(BUNDLE_DISCOVERY_KEY, checked);
            edit.apply();
            if(checked) {
                hostEditText.setEnabled(false);
                hostEditText.setText("");
            } else {
                hostEditText.setEnabled(true);
                hostEditText.setText(prefs.getString(HOSTNAME_KEY, _app.getHost()));
            }
        });

        hostEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String host = hostEditText.getText().toString().trim();
                _app.setHost(host);
                _app.setUseDiscovery(false);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putString(HOSTNAME_KEY, host);
                edit.putBoolean(BUNDLE_DISCOVERY_KEY, false);
                edit.apply();
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            @Override
            public void onTextChanged(CharSequence s, int start, int count, int after){}
        });

        ArrayAdapter<String> modeAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, DELIVERY_MODE_DESC);
        modeSpinner.setAdapter(modeAdapter);
        modeSpinner.setOnItemSelectedListener(new android.widget.AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _app.setDeliveryMode(DELIVERY_MODES[(int) id]);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putInt(BUNDLE_KEY_MODE, (int) id);
                edit.apply();
            }
            @Override public void onNothingSelected(AdapterView<?> arg0) { }
        });

        modeSpinner.setSelection(0);
        _app.setDeliveryMode(DELIVERY_MODES[(int)modeSpinner.getSelectedItemId()]);

        timeoutSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromThumb) {
                timeoutTextView.setText(String.format(java.util.Locale.getDefault(), "%.1f", progress / 1000.0));
                _app.setTimeout(progress);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        if ( ! prefs.getBoolean(BUNDLE_DISCOVERY_KEY, false)) {
            hostEditText.setText(prefs.getString(HOSTNAME_KEY, _app.getHost()));
        } else {
            _app.setUseDiscovery(true);
            useDiscoveryCheckBox.setChecked(true);
            hostEditText.setEnabled(false);
            hostEditText.setText("");
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        final CheckBox useDiscoveryCheckBox = (CheckBox)findViewById(R.id.useDiscovery);
        final EditText hostEditText = (EditText)findViewById(R.id.host);
        final SeekBar timeoutSeekBar = (SeekBar)findViewById(R.id.timeout);
        final Spinner modeSpinner = findViewById(R.id.mode);
        String hostIP = savedInstanceState.getString(BUNDLE_KEY_HOSTIP);

        timeoutSeekBar.setProgress(savedInstanceState.getInt(BUNDLE_KEY_TIMEOUT));
        modeSpinner.setSelection(savedInstanceState.getInt(BUNDLE_KEY_MODE));
        useDiscoveryCheckBox.setChecked(savedInstanceState.getBoolean(BUNDLE_DISCOVERY_KEY));
        hostEditText.setText(hostIP);

        if(useDiscoveryCheckBox.isChecked()) {
            _app.setUseDiscovery(true);
            hostEditText.setEnabled(false);
            hostEditText.setText("");
        } else {
            _app.setUseDiscovery(false);
            hostEditText.setEnabled(true);
            hostEditText.setText(hostIP);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        final CheckBox useDiscoveryCheckBox = (CheckBox)findViewById(R.id.useDiscovery);
        final SeekBar timeoutSeekBar = findViewById(R.id.timeout);
        final EditText hostEdit = (EditText) findViewById(R.id.host);
        outState.putInt(BUNDLE_KEY_TIMEOUT, timeoutSeekBar.getProgress());
        outState.putBoolean(BUNDLE_DISCOVERY_KEY, useDiscoveryCheckBox.isChecked());
        outState.putString(BUNDLE_KEY_HOSTIP,  hostEdit.getText().toString());

        // Clear the application handler. We don't want any further messages while
        // in the background.
        _app.setHandler(null);
    }
}