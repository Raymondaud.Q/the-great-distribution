# sudo apt install ettercap-graphical

* lancer ettercap et selectionner l'interface réseau wlan01.

* Lancer le scan des hôtes et les lister.

* Lancer `route -n` et trouver la l'ip de la passerelle par défaut ( qui est l'adresse du router/FreeBox )

* Ajouter l'Host correspondant au router comme 'Target 1' ( 192.168.1.254)

* Ajouter l'Host cible correspondant au client comme  'Target 2' ( Android phone : 192.168.1.16)

* Aller dans le menu MITM, cocher la case ARP Poisoning et valider

* Aller dans Pluggins > Manage Pluggins puis Double clique sur Check if poising had success

    - Si l'ARP poising est mis en place avec succès alors le message suivant devrait s'afficher dans la console :
        Activating chk_poison plugin...
        chk_poison: Checking poisoning status...
        chk_poison: Poisoning process successful!




# sudo apt install wireshark

* Lancer le serveur ICE

* Capturer les paquets sur l'interface wlan0

* Filtrer par requête TCP

* Lancer le client et faire une requête

    - L'appel vers l'API est visible :

        Operation Name : SoundCrud.getSoundList
        key : accessToken
        value : vcJwFHWfAZ4GZEds

    Ces informations pourraient déjà permettre d’arrêter les streams du clients et d’exécuter des requêtes en son nom à l'aide de la valeur d'accessToken ( qui est une sorte de cookie de session finalement )


    - La réponse est aussi visible en texte brut :

            0000   8b 00 00 00 01 01 04 12 54 72 6f 63 69 74 6f 73   ........Trocitos
            0010   5f 44 65 5f 4d 61 64 65 72 61 09 4c 61 5f 59 65   _De_Madera.La_Ye
            0020   67 72 6f 73 06 43 55 4d 42 49 41 0a 44 75 62 5f   gros.CUMBIA.Dub_
            0030   3d 5f 42 61 73 73 05 4b 61 6e 6b 61 03 44 55 42   =_Bass.Kanka.DUB
            0040   0d 43 68 6f 6f 73 65 5f 57 69 73 65 6c 79 14 43   .Choose_Wisely.C
            0050   68 61 6d 61 6e 5f 26 5f 43 72 75 64 6f 62 69 6c   haman_&_Crudobil
            0060   61 62 6f 03 44 55 42 0c 46 65 65 6c 5f 53 6f 5f   abo.DUB.Feel_So_
            0070   46 69 6e 65 12 4d 61 6e 75 64 69 67 69 74 61 6c   Fine.Manudigital
            0080   5f 26 5f 4d 65 73 68 03 44 55 42                  _&_Mesh.DUB

    Nous avons donc intercepté le trafic concernant la requête getSoundList() et sa réponse, qui est donc la liste des musiques car le trafic n'est pas chiffré !  
    Il serait aussi possible d'écouter le stream en cours du client en récupérant leurs liens de diffusion dans les paquets et en les écoutant à l'aide de VLC Media Player ou tout autre lecteur de flux audio sur réseau.  