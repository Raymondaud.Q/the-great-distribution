# Streaming Server
![Projet SOuP](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/raw/master/FR_PDF/Projet_SOuP.png)
## Dependencies 

You must install : 
* [IceZeroc](https://zeroc.com/downloads/ice)
* [libVlc](https://wiki.videolan.org/LibVLC_Tutorial/) 
* G++/C++ compiler  

### Linux Setup
On Ubuntu 20 based distros you can run the following commands :  

```
# G++/C++ INSTALL
sudo apt install build-essential

# ICE INSTALL
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv B6391CB2CFBA643D
sudo apt-add-repository "deb http://zeroc.com/download/ice/3.7/debian10 stable main"
sudo apt-get update
sudo apt-get install libzeroc-ice-dev libzeroc-ice3.7

# LIBVLC INSTALL
sudo apt install libvlc-dev
```
**You need to enable incoming traffic on the server's Firewall**

## Linux - Build & Run
Compilation steps are detailled [compiler.sh ShellScript](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/blob/master/ServerICE/compiler.sh)
### Build
In order to Build & Compile the server and the C++11 client you can run `./compiler.sh`

### Run the server
You can run the server with the following command: `./bin/server.exe`


### Run the client

* Launch `./bin/client.exe`
* Get a new AccessToken : `.`
* Get the sound list : `t`
* Search/Query Engine for media : 'q'
	* Select your query type by providing the type's `<index>` then type your query str
* Open a stream : `g` 
	* Start and play the stream by providing medium's `<index>`
	* Play/Pause the current stream : `p`
	* Stop the current stream : `s`
* Update medium information : `u`
	* Select the medium to update by providing medium's `<index>`
	* Enter your updated information
* Upload a medium : `F`
	* Enter medium information and give the local path 
	* Wait the uploading to finish
* Remove a medium : `r`
	* Remove the medium by providing medium's `<index>`
