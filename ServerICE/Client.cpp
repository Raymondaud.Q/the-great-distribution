// Edited By Quentin Raymondaud 
// Started from ZeroC cpp11 Discovery/hello example

#include <Ice/Ice.h>
#include "soundlib/SoundLibI.h"
#include "ice_output/SoundLibrary.h"
#include <fstream>      // ifstream
#include <vlc/vlc.h>

using namespace std;
using namespace SoundLibrary;

int run(const shared_ptr<Ice::Communicator>&);

int
main(int argc, char* argv[]) {
#ifdef ICE_STATIC_LIBS
	Ice::registerIceSSL();
	// Ice::registerIceDiscovery(false);
#endif
	int status = 0;
	try{
		// CommunicatorHolder's ctor initializes an Ice communicator,
		// and its dtor destroys this communicator.
		Ice::CommunicatorHolder ich(argc, argv, "config.client");
		// The communicator initialization removes all Ice-related arguments from argc/argv
		if(argc > 1) {
			cerr << argv[0] << ": too many arguments" << endl;
			status = 1;
		} else {
			status = run(ich.communicator());
		}
	} catch(const exception& ex) {
		cerr << argv[0] << ": " << ex.what() << endl;
		status = 1;
	}

	return status;
}

void menu();

int run(const shared_ptr<Ice::Communicator>& communicator) {
	//
	// Create a well-known proxy for the `hello' Ice object. A well-known proxy
	// only includes the Ice object identity. It's resolved using the Ice locator
	// implementation.
	//
	auto authenticator = Ice::checkedCast<AuthenticatorPrx>(communicator->propertyToProxy("Authenticator.Proxy"));
	auto twoway = Ice::checkedCast<SoundInterfacePrx>(communicator->propertyToProxy("SoundCrud.Proxy"));
	auto twowayVLC = Ice::checkedCast<PlayerInterfacePrx>(communicator->propertyToProxy("SoundPlayer.Proxy"));

	bool pause = false;
	if(!twoway) {
		cerr << "invalid proxy" << endl;
		return 1;
	}
	if (!twowayVLC) {
		cerr << "invalid VLCproxy" << endl;
		return 1;
	}

	// Secure connexion by default
	bool secure = true;
	twoway = twoway->ice_secure(secure);
	twowayVLC = twowayVLC->ice_secure(secure);
	authenticator = authenticator->ice_secure(secure);

	int timeout = -1;
	int delay = 0;
	bool streamOpened = false;
	libvlc_instance_t * inst;
	libvlc_media_player_t * mp;
	libvlc_media_t *m;

	menu();

	char c = 'x';
	vector<Sound> sound;
	// Request an access token from the server's authentication object.
	if ( authenticator != NULL ){
		string token = authenticator->getToken();
		cout << "Successfully retrieved access token: \"" << token << "\"" << endl;
		// Add the access token to the communicator's context, so it will be
		// sent along with every request made through it.
		communicator->getImplicitContext()->put("accessToken", token);
	} else {
		cout << " Error :  NULL Authenticator ! " << endl;
	}
				
	do {
		try {
			cout << "==> ";
			cin >> c;
			if(c == 'q'){
				int type = 0;
				cout 	<< " Search by :\n"
							<< "[0] - Title	\n"
							<< "[1] - Author \n"
							<< "[2] - Kind \n"
							<< "Give the query type index  : ";
				cin >> type;
				cin.ignore();
				string query = "";
				cout << "Type your request : ";
				getline(cin, query);
				cout << endl;
				twoway->soundQuery(type,query,sound);
				int index = 0;
				for (const SoundLibrary::Sound track : sound ){
					string trackInfo = to_string(index) + " | " + track.title + " - " + track.author + " #"+ track.kind;
					replace(trackInfo.begin(), trackInfo.end(), '_', ' ');
					cout << trackInfo << endl;
					index+=1;
				}
			} else if (c == 'g' && ! communicator->getImplicitContext()->get("accessToken").empty() ) {

				if ( ! sound.empty() ){
					string url = "";
					int index = 0;
					cout << " Give the media index to stream it : ";
					cin >> index;
					if ( streamOpened ){
						libvlc_media_player_stop (mp);
						libvlc_media_player_release (mp);
						libvlc_release (inst);
					}
			
					twowayVLC->playSound(communicator->getImplicitContext()->get("accessToken"), sound[index],url);
					cout << url << endl;
					inst = libvlc_new (0, NULL);
					m = libvlc_media_new_location (inst, ("http://localhost"+url).c_str() );
					/* Create a media player playing environement */
					mp = libvlc_media_player_new_from_media (m);
					/* No need to keep the media now */
					libvlc_media_release (m);
					/* play the media_player */
					libvlc_media_player_play (mp);
					streamOpened = true;
					sleep(1);
					menu();
				} else 
					cout << "Please, you must get the sound list with 't' before trying to stream a medium" << endl;

			} else if (c == 's' && streamOpened && ! communicator->getImplicitContext()->get("accessToken").empty() ) {
				twowayVLC->stopSound(communicator->getImplicitContext()->get("accessToken"));
				/* Stop playing */
				libvlc_media_player_stop (mp);
				/* Free the media_player */
				libvlc_media_player_release (mp);
				libvlc_release (inst);
				inst = NULL;
				mp = NULL;
				streamOpened = false;
				menu();
			} else if(c == 't'){
				twoway->getSoundList(sound);
				int index = 0;
				for (const SoundLibrary::Sound track : sound ){
					string trackInfo = to_string(index) + " | " + track.title + " - " + track.author + " #"+ track.kind;
					replace(trackInfo.begin(), trackInfo.end(), '_', ' ');
					cout << trackInfo<< endl;
					index+=1;
				}
			} else if(c == 'r'){
				if ( streamOpened ){
					libvlc_media_player_stop (mp);
					libvlc_media_player_release (mp);
					libvlc_release (inst);
					streamOpened = false;
				}
				int index = 0;
				cout << " Give the medium file index that for removal : ";
				cin >> index;
				twoway->removeSound(sound[index]);
			} else if(c == 'u'){
				if ( streamOpened ){
					libvlc_media_player_stop (mp);
					libvlc_media_player_release (mp);
					libvlc_release (inst);
					streamOpened = false;
				}
				string artist, title, kind;
				int index;
				cout << " Give the medium file index to update : ";
				cin >>  index;
				cin.ignore();
				cout << " Update Artist Name : ";
				getline(cin, artist);
				cout << " Update Title : ";
				getline(cin, title) ;
				cout << " Update Kind : ";
				getline(cin, kind);
				SoundLibrary::Sound renamed;
				renamed.title = title;  renamed.author = artist; renamed.kind=kind;
				twoway->renameSound(sound[index],renamed);
			} else if (c == 'p' && mp != NULL && inst != NULL && ! communicator->getImplicitContext()->get("accessToken").empty() ) {
				pause = !pause;
				if ( pause ){
					cout << "PAUSE ! " << endl;
					libvlc_media_player_pause (mp);
				} else {
					cout << "PLAY ! " << endl;
					libvlc_media_player_play (mp);
				}
				menu();
			} else if ( c == '.' ) {
				if ( streamOpened ){
					libvlc_media_player_stop (mp);
					libvlc_media_player_release (mp);
					libvlc_release (inst);
					streamOpened = false;
				}
				// Request an access token from the server's authentication object.
				if ( authenticator != NULL ){
					string token = authenticator->getToken();
					cout << "Successfully retrieved access token: \"" << token << "\"" << endl;
					// Add the access token to the communicator's context, so it will be
					// sent along with every request made through it.
					communicator->getImplicitContext()->put("accessToken", token);
				} else {
					cout << " Error :  NULL Authenticator ! " << endl;
				}
				
			} else if ( c == 'F') {
				string path, artist, title, kind;
				
				cin.ignore();
				cout <<  "Artist Name : ";
				getline(cin, artist);
				cout <<  "Title : ";
				getline(cin, title) ;
				cout <<  "Kind : ";
				getline(cin, kind);
				cout <<  "Local Path to medium: ";
				getline(cin, path);

				ifstream file(path, ifstream::binary);
				if ( file ){
					vector<char> buffer (1024,0); //reads only the first 1024 bytes
					int offset = 0;
					deque<future<int>> results;
					const int numRequests = 2;

					SoundLibrary::Sound sd;
					sd.title = title;
					sd.author = artist;
					sd.kind = kind;

					while(!file.eof()){
						file.read(buffer.data(), buffer.size());
						streamsize s=file.gcount();
						ByteSeq bs(buffer.begin(),buffer.begin()+s);
						// Send up to numRequests + 1 chunks asynchronously.
						// auto fut = batchOneway->uploadSoundAsync(sd, offset, bs); // Effectivement ça marche moins bien LOL
						auto fut = twoway->uploadSoundAsync(sd, offset, bs);
						results.push_back(move(fut));
						offset += s;
						// Once there are more than numRequests, wait for the least
						// recent one to complete.
						while(results.size() > numRequests) {
							results.front().get();
							results.pop_front();
						}
					}
					// Wait for any remaining requests to complete.
					while(!results.empty()) {
						results.front().get();
						results.pop_front();
					}
				}
			} else if(c == 'T') {
				if(timeout == -1){
					timeout = 2000;
				} else {
					timeout = -1;
				}

				twoway = twoway->ice_invocationTimeout(timeout);

				if(timeout == -1){
					cout << "timeout is now switched off" << endl;
				} else {
					cout << "timeout is now set to 2000ms" << endl;
				}
			} else if(c == 'P') {
				if(delay == 0){
					delay = 2500;
				} else {
					delay = 0;
				} 

				if(delay == 0){
					cout << "server delay is now deactivated" << endl;
				} else {
					cout << "server delay is now set to 2500ms" << endl;
				}
			} else if(c == 'S') {
				secure = !secure;
				twoway = twoway->ice_secure(secure);
				twowayVLC = twowayVLC->ice_secure(secure);
				authenticator = authenticator->ice_secure(secure);
				if(secure) {
					cout << "secure mode is now on" << endl;
				} else {
					cout << "secure mode is now off" << endl;
				}
			} else if(c == 'x') {
				// Nothing to do
			} else if(c == '?') {
				menu();
			} else {
				cout << "unknown command `" << c << "'" << endl;
				menu();
			}
		} catch(const Ice::Exception& ex) {
			cerr << ex << endl;
		}
	}
	while( cin.good() && c != 'x');

	return 0;
}

void menu() {
	cout <<
		"--OPTIONAL NOT TESTED--\n"
		"T: set a timeout\n"
		"P: set server delay\n\n"
		"### USAGE  ###\n"
		".: Change Access Token\n"
		"S: switch secure mode on/off\n"
		"q: Query Engine\n"
		"t: Get Sound List\n"
		"g: Stream Music from Sound List\n"
		"r: Remove File\n"
		"u: Update File informations\n"
		"p: Play/Pause Stream\n"
		"s: Stop Stream\n"
		"F: Upload File\n"
		"x: Exit\n"
		"?: Help\n";
}
