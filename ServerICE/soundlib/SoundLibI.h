// Quentin RAYMONDAUD

#ifndef SOUNDLIB_I_H
#define SOUNDLIB_I_H

#include "../ice_output/SoundLibrary.h"
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <string>
#include <Ice/Ice.h>
#include <algorithm>
#include <stdio.h>

class SoundLibI : public SoundLibrary::SoundInterface{
	
	public:
		virtual void getSoundList(  SoundLibrary::SoundList &result, const Ice::Current&) override;
		virtual void soundQuery( int queryType, std::string query, SoundLibrary::SoundList &result, const Ice::Current& c) override;
		virtual int uploadSound( SoundLibrary::Sound sound, int offset, Ice::ByteSeq chunk, const Ice::Current& ) override;
		virtual bool removeSound(SoundLibrary::Sound sound, const Ice::Current&) override;
		virtual bool renameSound(SoundLibrary::Sound sound, SoundLibrary::Sound newSound, const Ice::Current&) override; 
		virtual inline void shutdown(const Ice::Current& c) override {
			std::cout << "Shutting down..." << std::endl;
			c.adapter->getCommunicator()->shutdown();
		}

		inline int getDir (std::string dir, std::vector<std::string> &files) {
		    DIR *dp;
		    struct dirent *dirp;
		    if((dp  = opendir(dir.c_str())) == NULL) {
		        std::cout << "Error(" << errno << ") opening " << dir << std::endl;
		        return errno;
		    }
		    while ((dirp = readdir(dp)) != NULL) {
		        files.push_back(std::string(dirp->d_name));
		    }
		    closedir(dp);
		    return 0;
		}

		inline int write(std::string fileName, int offset, Ice::ByteSeq chunk, bool append=true ){
			auto mode = ( append==true ) ? ( std::ofstream::out | std::ofstream::app ) : std::ios_base::out ;
		    std::ofstream sw("./upload/mp3/" + fileName, mode );
		    if ( sw ){
		        long unsigned int chunkSize = chunk.size();
		        for(long unsigned int i = 0; i < chunkSize; i++)
		          sw << chunk[i];
		      	sw.close();
		        return offset;
		    }
		    return -1;
		}

		inline bool removeFile(std::string fileToRemove) { return ( remove( fileToRemove.c_str() ) != 0 ); }
		inline bool renameFile(std::string oldName, std::string newName) { return ( rename(oldName.c_str(), newName.c_str() ) != 0 ); }
};

#endif
