#include "SoundLibI.h"


void SoundLibI::getSoundList( SoundLibrary::SoundList &result, const Ice::Current&){
	result = std::vector<SoundLibrary::Sound>();

	std::vector<std::string> ls = std::vector<std::string>();
	this->getDir("./upload/mp3",ls);
	SoundLibrary::Sound s;
    for (const auto file : ls ){
        if ( file != "." && file != ".."){
            std::size_t found = file.find("-");
            std::size_t found2 = file.find("#");
            std::size_t found3 = file.find(".");

            if (found != std::string::npos) { s.author = file.substr(0,found); }
            else { s.author = file; }

            if (found2 != std::string::npos) { s.title = file.substr(found+1,found2-1-found); }
            else { s.title = "NA"; }

            if (found2 != std::string::npos) { s.kind = file.substr(found2+1, found3-found2-1); }
            else { s.kind = "NA"; }
            
            //std::cout << std::endl << s.title << s.author << s.kind << std::endl;
            
        	result.push_back(s);
        }
    }
    std::cout << std::endl << result.size() << " Track titles sent" << std::endl;
}

std::string toUpper(std::string str){
    std::string newStr;
    for (auto & c: str) newStr += toupper(c);
    return newStr;
}

void SoundLibI::soundQuery( int queryType, std::string query, SoundLibrary::SoundList &result, const Ice::Current& c){
    SoundLibrary::SoundList globalList;
    query = toUpper(query);
    std::replace( query.begin(), query.end(), ' ', '_' );    
    this->getSoundList(globalList, c);
    if ( queryType == 0 ){ // Search By Title
        for ( const auto sound : globalList ){
            std::string sTitle = toUpper(sound.title);
            if (sTitle.find(query) != std::string::npos) result.push_back(sound);
        }
    } else if ( queryType == 1) { // Search By Author
        for ( const auto sound : globalList ){
            std::string sAuthor = toUpper(sound.author);
            if (sAuthor.find(query) != std::string::npos) result.push_back(sound);
        }
    } else if ( queryType == 2) { // Search By Kind
        for ( const auto sound : globalList ){
            std::string sKind = toUpper(sound.kind);
            if (sKind.find(query) != std::string::npos) result.push_back(sound);
        }
    }
}

int SoundLibI::uploadSound( SoundLibrary::Sound sound, int offset, Ice::ByteSeq chunk, const Ice::Current& ){
    std::string fileName = sound.author + "-" + sound.title + "#" + sound.kind + ".mp3";
    std::replace( fileName.begin(), fileName.end(), ' ', '_' );               
    if ( offset == 0 ){
        std::cout << "Uploading new mp3" << std::endl;
        std::vector<std::string> mp3List = std::vector<std::string>();
        this->getDir("./upload/mp3", mp3List);
        if ( std::find( mp3List.begin(), mp3List.end(), fileName) != mp3List.end() )
            this->write(fileName, offset, chunk, false);
        else 
            this->write(fileName, offset, chunk);
    }
    return this->write(fileName, offset, chunk);
}


bool SoundLibI::renameSound( SoundLibrary::Sound sound, SoundLibrary::Sound newSound, const Ice::Current&){
	std::string oldName = "./upload/mp3/" + sound.author + "-" + sound.title + "#" + sound.kind + ".mp3";
	std::string newName = "./upload/mp3/" + newSound.author + "-" + newSound.title + "#" + newSound.kind + ".mp3";
	std::replace( oldName.begin(), oldName.end(), ' ', '_' );
	std::replace( newName.begin(), newName.end(), ' ', '_' );
	return renameFile(oldName, newName);
}

bool SoundLibI::removeSound( SoundLibrary::Sound sound, const Ice::Current& ){
	std::string path = "./upload/mp3/" + sound.author + "-" + sound.title + "#" + sound.kind + ".mp3";
	std::replace( path.begin(), path.end(), ' ', '_' );
	std::cout << path << " will be remove ! " << std::endl ;
	return removeFile(path);
}
