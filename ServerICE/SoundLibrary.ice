// Quentin RAYMONDAUD
// SLICE Interface ( ZeroC Ice Middleware )

#pragma once
module SoundLibrary {

	exception AuthorizationException {}

	exception TokenExpiredException extends AuthorizationException {}

	interface Authenticator {
		string getToken();
	}

	struct Sound {
		string title;
		string author;
		string kind;
	}

	sequence<Sound> SoundList;
	sequence<byte> ByteSeq;

	interface PlayerInterface {
		idempotent void playSound( string clientID, Sound sound, out string url );
		void stopSound( string clientID );
	}
	
	interface SoundInterface {
		idempotent void getSoundList( out SoundList result );
		int uploadSound( Sound sound, int offset, ByteSeq chunk );
		void shutdown();
		idempotent void soundQuery( int queryType, string query, out SoundList result );
		bool removeSound( Sound sound );
		idempotent bool renameSound( Sound sound, Sound newSound );
	}
}
