// Copyright (c) ZeroC, Inc. All rights reserved.
// https://github.com/zeroc-ice/ice-demos/blob/3.7/cpp11/Ice/interceptor/AuthenticatorI.h

#ifndef AUTHENTICATOR_I_H
#define AUTHENTICATOR_I_H

#include <chrono>
#include <mutex>
#include <random>
#include <map>
#include "../ice_output/SoundLibrary.h"

class AuthenticatorI : public SoundLibrary::Authenticator {

	public:
	    AuthenticatorI();
	    virtual std::string getToken(const Ice::Current&) override;
	    void validateToken(const std::string&);

	private:
	    std::mt19937_64 _rand;
	    std::map<std::string, std::chrono::time_point<std::chrono::steady_clock>> _tokenStore;
	    std::mutex _tokenLock;
};

#endif