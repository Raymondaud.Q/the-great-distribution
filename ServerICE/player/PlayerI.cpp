#include "PlayerI.h"

void PlayerI::playSound( std::string clientID, SoundLibrary::Sound sound, std::string &soundUrl, const Ice::Current& c){
	/* Load the VLC engine if not already loaded*/

	this->stopSound(clientID, c);
	if ( ! this->inst)
		this->inst = libvlc_new(0, NULL);

	std::string mediaName =  sound.author + "-" + sound.title + "#"+ sound.kind;
	std::replace( mediaName.begin(), mediaName.end(), ' ', '_' );  
	std::string path = "./upload/mp3/" + mediaName + ".mp3";
	std::string streamUrl = "#transcode{acodec=mp3,ab=128,channels=2,samplerate=44100}:http{dst=:8000/" + clientID + "/" +  sound.author + sound.title + sound.kind + ".mp3, name="+clientID+"}";
	if(libvlc_vlm_add_broadcast( this->inst, clientID.c_str(), path.c_str(), streamUrl.c_str(), 0, NULL, true, true)!=0){
		std::cerr << std::endl << " ERROR : Can't Open Stream" << std::endl;
		soundUrl = "ERROR";
		return ;
	}
	libvlc_vlm_play_media( this->inst, clientID.c_str());
	soundUrl = ":8000/" + clientID + "/" + sound.author + sound.title + sound.kind + ".mp3";
	std::cout << std::endl << clientID << "'s Stream Started on " + soundUrl << std::endl;
	this->streamUrls[clientID] = soundUrl;
}

void PlayerI::stopSound( std::string clientID, const Ice::Current& ) {
	if ( this->inst ){
		libvlc_vlm_del_media(this->inst, clientID.c_str() );
		this->streamUrls[clientID].clear(); 
		std::cout << std::endl << clientID << "'s Stream STOPPED SUCCESSFULLY" << std::endl;
	} else {
		std::cerr << std::endl << " ERROR : Can't stop unexisting Stream" << std::endl;
	}
}

