
#ifndef SOUNDPLAY_I_H
#define SOUNDPLAY_I_H

#include "../ice_output/SoundLibrary.h"
#include <ostream>
#include <Ice/Ice.h>
#include <vlc/vlc.h>
#include <string>
#include <unordered_map>
#include <iostream>
#include <cassert>

class PlayerI : public SoundLibrary::PlayerInterface {

	// static auto vive_la_crotte_API = libvlc_new(0,NULL);


	// libvlc_vlm_add_broadcast(	c->getVlc(),
	//								idClient.c_str(),
	//								path.c_str(),
	//								c->getStreamStr().c_str(),
	//								0, NULL, true, false);
	private :
		libvlc_instance_t * inst;
		std::unordered_map<std::string, std::string> streamUrls; // key = clientID, value = stream mrl

	public : 
		virtual void playSound( std::string clientID, SoundLibrary::Sound sound, std::string &soundUrl, const Ice::Current& c) override;
		virtual void stopSound( std::string clientID, const Ice::Current& ) override;
		// virtual void pauseSound( std::string clientID, const Ice::Current& ) override;
};

#endif