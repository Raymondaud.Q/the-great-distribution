//
// Copyright (c) ZeroC, Inc. All rights reserved.
//

#include <Ice/Ice.h>
#include "soundlib/SoundLibI.h"
#include "player/PlayerI.h"
#include "authenticator/AuthenticatorI.h"
// #include "query_analyzer/ComplexQueryI.h"
#include "interceptor/InterceptorI.h"
#include <unordered_set>
using namespace std;

int main(int argc, char* argv[])
{
#ifdef ICE_STATIC_LIBS
    Ice::registerIceSSL();
    //Ice::registerIceDiscovery(false);
#endif
    int status = 0;
    try {
        // CtrlCHandler must be created before the communicator or any other threads are started
        Ice::CtrlCHandler ctrlCHandler;

        // CommunicatorHolder's ctor initializes an Ice communicator,
        // and its dtor destroys this communicator.
        Ice::CommunicatorHolder ich(argc, argv, "config.server");
        auto communicator = ich.communicator();

        ctrlCHandler.setCallback(
            [communicator](int)
            {
                communicator->shutdown();
            });

        // The communicator initialization removes all Ice-related arguments from argc/argv
        if (argc > 1) {
            cerr << argv[0] << ": too many arguments" << endl;
            status = 1;
        } else {
            // Create an object adapter for the authenticator.
            auto authenticatorAdapter = communicator->createObjectAdapter("Authenticator");
            auto authenticator = make_shared<AuthenticatorI>();
            authenticatorAdapter->add(authenticator, Ice::stringToIdentity("authenticator"));
            authenticatorAdapter->activate();

            // Set of all the operations to require authorization for : 
            unordered_set<string> securedOperations({ "playSound", "stopSound" });

            auto SoundCrud_ADAPTER = communicator->createObjectAdapter("SoundCrud");
            auto SoundPlayer_ADAPTER = communicator->createObjectAdapter("SoundPlayer");
            auto lib = make_shared<SoundLibI>();
            auto player = make_shared<PlayerI>();
            // auto queryLexer = make_shared<ComplexQueryAnalyzerI>();
            auto interceptor = make_shared<InterceptorI>(move(player), authenticator, move(securedOperations));

            SoundPlayer_ADAPTER->add(interceptor, Ice::stringToIdentity("soundplayer"));
            SoundCrud_ADAPTER->add(lib, Ice::stringToIdentity("soundcrud"));

            SoundPlayer_ADAPTER->activate();
            SoundCrud_ADAPTER->activate();
            
            communicator->waitForShutdown();
        }
    } catch(const std::exception& ex) {
        cerr << ex.what() << endl;
        status = 1;
    }
    return status;
} 