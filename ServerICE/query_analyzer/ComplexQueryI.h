// Quentin RAYMONDAUD


#ifndef COMPLEXQUERY_I_H
#define COMPLEXQUERY_I_H

#include "../ice_output/SoundLibrary.h"
#include <Ice/Ice.h>
#include <sys/types.h>
#include <string>
#include <vector>

class ComplexQueryAnalyzerI : public SoundLibrary::ComplexQueryInterface {
	public :
		virtual void analyze( std::string query, std::string &result, const Ice::Current& c) override;
};

#endif