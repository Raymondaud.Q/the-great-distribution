#include "ComplexQueryI.h"


// ------ Listing Features ------------
const std::string listBy [] = {"recherche par","cherche par","lister par","liste par"};
const std::string listAll [] = {"liste","liste tou"};
const std::string byAuthor [] = { "auteur","nom","artiste","pseudo"};
const std::string byKind [] = {"genre","style","type"};
const std::string byTitle [] = {"titre"};


void ComplexQueryAnalyzerI::analyze( std::string query, std::string &result, const Ice::Current& c){

	std::cout << " Requete = " << query << std::endl;
	// List By
	for ( std::string orderListBy : listBy ){
		if ( query.find(orderListBy) != -1 ){
			for ( std::string author : byAuthor ){
				if ( query.find(author) !=  -1 ){
					result = "ListBy:Author";
					return;
				}
			}

			for ( std::string kind : byKind ){
				if ( query.find(kind) != -1){
					result ="ListBy:Kind";
					return;
				}
			}

			for ( std::string title : byTitle){
				if ( query.find(title) != -1 ){
					result ="ListBy:Title";
					return;
				}
			}
		}
	}

	// List All
	for ( std::string orderListAll : listAll ){
		if ( query.find(orderListAll) !=-1 ){
			result ="ListAll";
			return;
		}
	}
	
}
