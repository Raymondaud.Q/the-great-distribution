mkdir -p ice_output
mkdir -p bin
mkdir -p obj
slice2cpp SoundLibrary.ice --output-dir ice_output --include-dir ice_output
g++ $(pkg-config --cflags libvlc)  -I. -DICE_CPP11_MAPPING  -c ice_output/SoundLibrary.cpp authenticator/AuthenticatorI.cpp interceptor/InterceptorI.cpp soundlib/SoundLibI.cpp player/PlayerI.cpp Server.cpp Client.cpp
mv *.o obj 
g++ -o bin/server.exe obj/SoundLibrary.o obj/AuthenticatorI.o obj/InterceptorI.o obj/SoundLibI.o obj/PlayerI.o obj/Server.o $(pkg-config --libs libvlc) -lIce++11 -lpthread 
g++ -o bin/client.exe obj/SoundLibrary.o obj/AuthenticatorI.o obj/InterceptorI.o obj/SoundLibI.o obj/PlayerI.o obj/Client.o $(pkg-config --libs libvlc) -lIce++11 -lpthread
