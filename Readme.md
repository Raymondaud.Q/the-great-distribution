# The Great Distribution : A Distributed Streaming Software

This repository provides a simplistic and fully-functional media streaming server.  
I would like to give a special thanks to the great & kind [Jose AKA Pepone](https://github.com/pepone) who helped me through [discussions](https://github.com/zeroc-ice/ice-demos/discussions) and [ICE demos](https://github.com/zeroc-ice/ice-demos) to deal with ZeroC ICE Middleware technology.

![Presentation](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/raw/master/FR_PDF/The_Great_Architecture.png)


## Available products

* [Server : Ice Zeroc with C++11 Mapping](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/ServerICE) 
* [CLI Client :  Ice Zeroc with C++11 Mapping](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/blob/master/ServerICE/Client.cpp)
* [Client : Ice Zeroc with Android Java Mapping](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/Client)
* [Vocal Cmd Server : Docker + Flask + Google SpeechRecognition](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/ServerVOCMD)

## How to use ServerICE & CLI Client ?

[Instructions are available in the ReadMe of the Server directory](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/ServerICE)  

You can also deploy it using the deploy script :  
* `./deploy_ice.sh`  
and launch a Client in another termial :  
* `./ServerICE/bin/client.exe`  

## How to use ServerVOCMD ?

[Instructions are available in the ReadMe of the Server directory](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/ServerVOCMD)

You can also deploy it using the deploy script :   
* `./deploy_vocmd.sh`  
and test API on these routes :  
* http://localhost:5000/ = QueryLexer - NLP  
* http://localhost:7000/ = Speech To Text - SR  

## How to use the Android Client ?

[Instructions are available in the ReadMe of the Client directory](https://gitlab.com/Raymondaud.Q/the-great-distribution/-/tree/master/Client)  
